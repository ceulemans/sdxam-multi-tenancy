<?php

namespace App\Filament\Admin\Pages;

use Filament\Forms\Form;
use Filament\Pages\Auth\Login as FilamentLogin;

class Login extends FilamentLogin
{
    public function form(Form $form): Form
    {
        return parent::form($form);
    }

    protected function getFormActions(): array
    {
        return parent::getFormActions();
    }
}
