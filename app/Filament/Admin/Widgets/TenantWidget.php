<?php

namespace App\Filament\Admin\Widgets;

use App\Models\Tenant;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Stat;
use Illuminate\Support\Facades\DB;

class TenantWidget extends BaseWidget
{
    protected function getStats(): array
    {
        $avgTenantUsers = cache()->remember(
            key: TenantWidget::class.'::avgTenantUsers',
            ttl: now()->addMinute(),
            callback: function () {
                return DB::query()
                    ->from(Tenant::query()->withCount('users'), 'counts')
                    ->avg('users_count');
            }
        );

        return [
            Stat::make('Tenants', Tenant::count()),
            Stat::make('Users/tenant', number_format($avgTenantUsers, 1)),
        ];
    }
}
