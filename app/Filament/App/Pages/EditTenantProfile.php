<?php

namespace App\Filament\App\Pages;

use App\Enums\UserRole;
use App\Models\Tenant;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Pages\Tenancy\EditTenantProfile as EditTenantProfileBase;
use Filament\Tables\Actions\EditAction;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Concerns\InteractsWithTable;
use Filament\Tables\Contracts\HasTable;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Table;

/**
 * @property-read Tenant $tenant
 */
class EditTenantProfile extends EditTenantProfileBase implements HasTable
{
    use InteractsWithTable;

    protected static ?string $navigationIcon = 'heroicon-o-document-text';

    protected static string $view = 'filament.app.pages.edit-tenant-profile';

    public static function getLabel(): string
    {
        return 'Edit Tenant';
    }

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make('Tenant Information')
                    ->schema([
                        TextInput::make('name')
                            ->maxLength(255)
                            ->helperText('The name of the tenant.'),
                        TextInput::make('slug')
                            ->disabled()
                            ->helperText('Contact your administrator to change this.'),
                    ]),
            ]);
    }

    public function table(Table $table): Table
    {
        return $table
            ->heading('Members')
            ->relationship(fn () => $this->tenant->users())
            ->columns([
                TextColumn::make('name')
                    ->searchable()
                    ->forceSearchCaseInsensitive(),
                TextColumn::make('email'),
                TextColumn::make('role'),
            ])
            ->filters([
                SelectFilter::make('role')
                    ->options(UserRole::class),
            ])
            ->actions([
                EditAction::make()
                    ->form([
                        TextInput::make('name'),
                        TextInput::make('email')
                            ->email(),
                        Select::make('role')
                            ->options(UserRole::class),
                    ]),
            ]);
    }
}
