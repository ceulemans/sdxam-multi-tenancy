<?php

namespace App\Models;

use Filament\Models\Contracts\FilamentUser;
use Filament\Models\Contracts\HasTenants;
use Filament\Panel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements FilamentUser, HasTenants
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function tenants(): BelongsToMany
    {
        return $this->belongsToMany(Tenant::class)
            ->withTimestamps()
            ->using(TenantUser::class)
            ->withPivot([
                'role',
            ]);
    }

    public function canAccessPanel(Panel $panel): bool
    {
        return User::query()
            ->orderBy('id')
            ->value('id') === $this->getKey();
    }

    public function canAccessTenant(Model $tenant): bool
    {
        return $this->tenants->contains($tenant) || $this->id === 1;
    }

    public function getTenants(Panel $panel): array|Collection
    {
        return $this->tenants;
    }
}
