<?php

namespace App\Models;

use App\Enums\UserRole;
use Illuminate\Database\Eloquent\Relations\Pivot;

class TenantUser extends Pivot
{
    public $incrementing = true;

    protected $casts = [
        'role' => UserRole::class,
    ];
}
