<?php

declare(strict_types=1);

namespace App\Enums;

use Filament\Support\Contracts\HasLabel;

enum UserRole: string implements HasLabel
{
    case Admin = 'admin';
    case Assessor = 'assessor';

    public function getLabel(): ?string
    {
        return ucfirst($this->value);
    }
}
